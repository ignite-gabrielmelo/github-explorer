# Primeira aula ignite - React
Configuração de todo ambiente react na mão

## 🏁 Hot to run
```bash
yarn dev
```

# 🚀 Stacks
- React
- React-dom

## ⚙️ Tools
- Babel
- Webpack
- Webpack-dev-server
- SASS